
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import steps.SignUpSteps;

@RunWith(SerenityRunner.class)
public class SignUpTest {

    @Steps
    SignUpSteps steps;

    @Managed(driver = "chrome")
    WebDriver driver;

/*    @BeforeClass
    public static void setup() {
        //System.setProperty("webdriver.chrome.driver", "C:\\Users\\MsRig\\Downloads\\chromedriver.exe");
        //System.setProperty("webdriver.gecko.driver", "C:\\Users\\MsRig\\Downloads\\geckodriver.exe");
        baseUrl = "https://dev.amayama.com/en";
        browser = "chrome";
    } */

    @Test
    public void emptyLogin() {
        steps.open_page();
        steps.open_login();
        steps.click_login_btn();
        steps.error_text_should_be("Please, allow us to use cookies and provided account " +
                "information for the authentication purposes and to contact you regarding your inquiries.");
         }

    @Test
    public void successLogin() {
        steps.open_page();
        steps.open_login_window();
        steps.set_values("burtovaya@vl.ru", "2549524435");
        steps.is_login();
    }
}
