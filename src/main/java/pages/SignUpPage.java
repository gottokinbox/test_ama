package pages;

import com.codeborne.selenide.SelenideElement;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SignUpPage extends PageObject {

    private By loginEmail = By.xpath("//div[contains(@class, 'tab login')]//input[@name='email']");
    private By loginPassword = By.xpath("//div[contains(@class, 'tab login')]//input[@name='password']");
    private By loginButton = By.xpath("//div[contains(@class, 'tab login')]//button[@class='submit']");
    private By loginCheckbox = By.xpath("//div[contains(@class, 'tab login')]//input[@type='checkbox']");
    private By fieldError = By.xpath("//*[contains(@class, 'tab login')]//div[contains(@class, 'popover-content')]");

    public SignUpPage setEmail(String email) {
        $(loginEmail).sendKeys(email);
        return this;
    }

    public SignUpPage setPass(String pass) {
        $(loginPassword).sendKeys(pass);
        return this;
    }

    public SignUpPage clickCheckBox() {
        if ($(loginCheckbox).isDisplayed())
        {
            $(loginCheckbox).click();
            return this;
        }
        else return this;
    }

    public void clickLoginBtn() {
        $(loginButton).click();
    }

    public String getError()  {
        return $(fieldError).getText();
    }

}
