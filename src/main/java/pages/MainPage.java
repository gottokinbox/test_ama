package pages;

import com.codeborne.selenide.SelenideElement;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;


@DefaultUrl("https://dev.amayama.com/en")
public class MainPage extends PageObject {

    By searchField = By.xpath("//input[@class='search__input']");
    By searchButton = By.xpath("//button[@class='search__submit']");
    By login = By.xpath("//a[contains(@class, 'login-link')]");
    By signUp = By.xpath("//a[.='Sing Up']");
    By profile = By.xpath("//*[@class='top-line']//div[contains(@class, 'personal')]/div[contains(@class, 'dropdown_click')]");


    public MainPage openLoginWindow() {
        $(login).click();
        return this;
    }

    public MainPage openSignupWindow() {
        $(signUp).click();
        return this;
    }

    public boolean isLogin() {
        //WebElement explicitWait = (new WebDriverWait(driver, 20))
          //      .until(ExpectedConditions.presenceOfElementLocated(profile));
        return $(profile).isDisplayed();
    }



}
