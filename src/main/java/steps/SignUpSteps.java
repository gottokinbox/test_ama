package steps;

import net.thucydides.core.annotations.Step;
import org.assertj.core.api.Assertions;
import pages.MainPage;
import pages.SignUpPage;

public class SignUpSteps {
    MainPage main_page;
    SignUpPage page;

    @Step
    public void open_page() {
        main_page.open();
    }

    @Step("открываем модалку")
    public void open_login(){
        main_page.openLoginWindow();
    }

    @Step
    public void click_login_btn() {
        page.clickLoginBtn();
    }

    @Step
    public String get_error_text() {
        return page.getError();
    }

    @Step
    public void error_text_should_be(String text) {
        Assertions.assertThat(page.getError().equals(text))
                .as("wrong error text");
    }

    @Step
    public void open_login_window() {
        main_page.openLoginWindow();
    }

    @Step
    public void set_values(String email, String password) {
        page.setEmail(email)
            .setPass(password)
            .clickCheckBox()
            .clickLoginBtn();
    }

    @Step
    public void is_login() {
        Assertions.assertThat(main_page.isLogin());
    }
}
